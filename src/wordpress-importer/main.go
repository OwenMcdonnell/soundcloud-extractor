package main

import (
	"encoding/json"
	"flag"
	"github.com/robbiet480/go-wordpress"
	"log"
	"os"
	"soundcloud-extractor/src/commons"
	"sync"
)

var (
	version string // set at build time
	commit  string // set at build time
	date    string // set at build time

	wpUser      string // flag
	wpPassword  string // flag
	fileBaseDir string // flag

	mimeTypes = map[string]string{
		audioMediaType: "audio/mpeg",
		imageMediaType: "image/jpeg",
	}
)

const (
	wordpressURL   = "https://dev.station-millenium.com/wp-json/"
	audioMediaType = "audio"
	imageMediaType = "image"
)

type mediaCheck struct {
	searchTerms,
	mediaType,
	filePath string
	mediaIDInterface commons.MediaID
}

func init() {
	flag.StringVar(&wpUser, "wpUser", "", "Wordpress user")
	flag.StringVar(&wpPassword, "wpPassword", "", "Wordpress password")
	flag.StringVar(&fileBaseDir, "fileBaseDir", commons.FileBaseDirDefault, "File base dir")
}

func main() {
	log.Printf("Version : %s - Commit : %s - Date : %s", version, commit, date)
	flag.Parse()
	if wpUser == "" || wpPassword == "" {
		flag.PrintDefaults()
		log.Fatalln("Missing parameters")
	}

	commons.InitDir(fileBaseDir)

	playlists := unmarshallData()
	playlistsDest := playlists

	var mediaCheckWG sync.WaitGroup
	var mediaCheckChan = make(chan mediaCheck)
	mediaCheckWG.Add(3)
	for i := 0; i < 3; i++ {
		go createMediaIfNotExisting(mediaCheckChan, &mediaCheckWG, i)
	}

	for index, playlist := range playlists {
		mediaCheckChan <- mediaCheck{
			playlist.Title, imageMediaType, playlist.ArtworkFile, &playlistsDest[index],
		}
		//for _, track := range playlist.Tracks {
		//	mediaCheckChan <- mediaCheck{
		//		track.Title, imageMediaType, track.ArtworkFile, track,
		//	}
		//	mediaCheckChan <- mediaCheck{
		//		track.Title, audioMediaType, track.SoundFile, track,
		//	}
		//}
	}

	close(mediaCheckChan)
	mediaCheckWG.Wait()
	log.Println("Media check done")

	if jsonFile, err := os.Create(commons.JSONFilePath); err == nil {
		json.NewEncoder(jsonFile).Encode(playlistsDest)
		log.Printf("File writed at %s\n", commons.JSONFilePath)
	} else {
		panic(err)
	}
}

func unmarshallData() []commons.PlaylistType {
	var playlists []commons.PlaylistType
	if playlistFile, err := os.Open(commons.JSONFilePath); err == nil {
		json.NewDecoder(playlistFile).Decode(&playlists)
	} else {
		panic(err)
	}
	return playlists
}

func getWPClient() *wordpress.Client {
	tp := wordpress.BasicAuthTransport{
		Username: wpUser,
		Password: wpPassword,
	}

	client, err := wordpress.NewClient(wordpressURL, tp.Client())
	if err != nil {
		panic(err)
	}
	return client
}
