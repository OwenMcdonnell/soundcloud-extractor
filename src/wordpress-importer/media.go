package main

import (
	"github.com/robbiet480/go-wordpress"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
	"context"
	"fmt"
	"strings"
)

const imageName = "%s.jpeg"

func createMediaIfNotExisting(mediaCheckChan <-chan mediaCheck, mediaCheckWG *sync.WaitGroup, id int) {
	defer mediaCheckWG.Done()
	log.Printf("Starting media check worker id %d\n", id)
	for check := range mediaCheckChan {
		if check.filePath != "" {
			mediaID := checkMediaExists(check.searchTerms, check.mediaType)
			if mediaID == 0 {
				mediaID = createMedia(check)
			}

			if check.mediaType == imageMediaType {
				check.mediaIDInterface.SetImageMediaID(mediaID)
			} else {
				check.mediaIDInterface.SetSoundMediaID(mediaID)
			}
		}
	}
	log.Printf("Stopping media check id %d\n", id)
}

func createMedia(media mediaCheck) int {
	// prepare file to upload
	file, err := os.Open(media.filePath)
	defer file.Close()
	if err != nil {
		log.Fatalf("Failed to open media file %s to upload: %v", media.filePath, err.Error())
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Failed to read media file %s to upload: %v", media.filePath, err.Error())
	}

	ctx := context.Background()
	mediaCreated, resp, err := getWPClient().Media.Create(ctx, &wordpress.MediaUploadOptions{
		Filename:    fmt.Sprintf(imageName, strings.Replace(media.searchTerms, "/", "-", -1)),
		ContentType: mimeTypes[media.mediaType],
		Data:        fileContents,
	})

	if err != nil || resp.StatusCode != http.StatusCreated {
		panic(err)
	}

	log.Printf("Created media %s (%s) - id %d", mediaCreated.Title, mediaCreated.Slug, mediaCreated.ID)
	return mediaCreated.ID
}

func checkMediaExists(search, mediaType string) int {
	ctx := context.Background()
	medias, resp, err := getWPClient().Media.List( ctx, &wordpress.MediaListOptions{
		MediaType: mediaType,
		ListOptions: wordpress.ListOptions{
			Search: formatMediaName(search),
		},
	})
	if err != nil || resp.StatusCode != http.StatusOK {
		panic(err)
	}

	if len(medias) == 0 || len(medias) > 1 {
		log.Printf("Erronous media length found %d for search %s\n", len(medias), search)
		return 0
	}

	log.Printf("Found id %d for search %s", medias[0].ID, medias[0].Slug)
	return medias[0].ID
}

func formatMediaName(mediaName string) string {
	return strings.Replace(strings.Replace(mediaName, "/", "-", -1), " ", "-", -1)
}