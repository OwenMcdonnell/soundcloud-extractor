package commons

const (
	//FileBaseDirDefault base dir of all files
	FileBaseDirDefault = "/tmp/soundcloud-extractor"
)

//MediaID set the ID of wordpress medias for types
type MediaID interface {
	SetImageMediaID(id int)
	SetSoundMediaID(id int)
}

//SetImageMediaID set the image media ID for playlist
func (playlist *PlaylistType) SetImageMediaID(id int) {
	playlist.ArtworkMediaID = id
}

//SetSoundMediaID do nothing - playlist has no sound media
func (playlist *PlaylistType) SetSoundMediaID(id int) {}

//SetImageMediaID set the image media ID for tracks
func (track *TrackType) SetImageMediaID(id int) {
	track.ArtworkMediaID = id
}

//SetSoundMediaID set the sound media ID for tracks
func (track *TrackType) SetSoundMediaID(id int) {
	track.SoundMediaID = id
}

// PlaylistType playlist type
type PlaylistType struct {
	ID             uint64
	Title          string
	ArtworkURL     string
	Tracks         []*TrackType
	ArtworkFile    string
	ArtworkMediaID int
}

//TrackType the track type
type TrackType struct {
	ID             uint64
	Title          string
	ArtworkURL     string
	SoundURL       string
	ArtworkFile    string
	SoundFile      string
	ArtworkMediaID int
	SoundMediaID   int
}
