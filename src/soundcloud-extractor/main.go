package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/yanatan16/golang-soundcloud/soundcloud"
	"io"
	"log"
	"net/http"
	"os"
	"soundcloud-extractor/src/commons"
	"sync"
)

const (
	soundCloudPlaylistTrackURL = "https://api.soundcloud.com/playlists/%d/tracks/?client_id=%s&limit=200"
	dirPerm                    = 0755
	trackSoundURL              = "%s?client_id=%s"
)

var (
	version string // set at build time
	commit  string // set at build time
	date    string // set at build time

	soundCloudUser     uint64 // flag
	soundCloudClientID string // flag
	fileBaseDir        string // flag
)

type downloadableFile struct {
	filePath, fileURL string
}

func init() {
	flag.Uint64Var(&soundCloudUser, "soundCloudUser", 0, "Soundcloud user ID")
	flag.StringVar(&soundCloudClientID, "soundCloudClientID", "", "Soundcloud client ID - API token")
	flag.StringVar(&fileBaseDir, "fileBaseDir", commons.FileBaseDirDefault, "File base dir")
}

func main() {
	log.Printf("Version : %s - Commit : %s - Date : %s", version, commit, date)
	flag.Parse()
	if soundCloudUser == 0 || soundCloudClientID == "" {
		flag.PrintDefaults()
		log.Fatalln("Missing parameters")
	}

	commons.InitDir(fileBaseDir)

	authenticatedAPI := &soundcloud.Api{
		ClientId: soundCloudClientID,
	}

	if _, err := os.Stat(fileBaseDir); os.IsNotExist(err) {
		err = os.Mkdir(fileBaseDir, dirPerm)
		if err != nil {
			panic(err)
		}
	}

	playlistsList := make([]*commons.PlaylistType, 0)

	var downloadWg sync.WaitGroup
	var downloadChan = make(chan downloadableFile)
	downloadWg.Add(3)
	for i := 0; i < 3; i++ {
		go downloadFile(downloadChan, &downloadWg, i)
	}

	if playlists, err := authenticatedAPI.User(soundCloudUser).Playlists(nil); err == nil {
		for _, playlist := range playlists {
			log.Println("Playlist " + playlist.Title)
			playlistItem := commons.PlaylistType{
				playlist.Id,
				playlist.Title,
				playlist.ArtworkUrl,
				nil, "", 0,
			}
			createPlaylistDir(playlistItem)
			playlistsList = append(playlistsList, &playlistItem)

			if playlistItem.ArtworkURL != "" {
				playlistItem.ArtworkFile = fmt.Sprintf(commons.PlaylistImageFile, playlistItem.ID, playlistItem.ID)
				downloadChan <- downloadableFile{
					playlistItem.ArtworkFile,
					playlistItem.ArtworkURL,
				}
			}
			getTracksOfPlaylist(&playlistItem, *playlist, downloadChan)
		}
	} else {
		panic(err)
	}

	close(downloadChan)
	downloadWg.Wait()
	if jsonFile, err := os.Create(commons.JSONFilePath); err == nil {
		json.NewEncoder(jsonFile).Encode(playlistsList)
		log.Printf("File writed at %s\n", commons.JSONFilePath)
	} else {
		panic(err)
	}
}

func createPlaylistDir(playlistItem commons.PlaylistType) {
	if _, err := os.Stat(fmt.Sprintf(commons.PlaylistDir, playlistItem.ID)); os.IsNotExist(err) {
		err = os.Mkdir(fmt.Sprintf(commons.PlaylistDir, playlistItem.ID), dirPerm)
		if err != nil {
			panic(err)
		}
	}
}

func createTrackDir(playlistItem commons.PlaylistType, track commons.TrackType) {
	if _, err := os.Stat(fmt.Sprintf(commons.TrackDir, playlistItem.ID, track.ID)); os.IsNotExist(err) {
		err = os.Mkdir(fmt.Sprintf(commons.TrackDir, playlistItem.ID, track.ID), dirPerm)
		if err != nil {
			panic(err)
		}
	}
}

func getTracksOfPlaylist(playlistItem *commons.PlaylistType, playlist soundcloud.Playlist, downloadChan chan<- downloadableFile) {
	log.Println("Tracks " + playlist.Title)
	tracksList := make([]*commons.TrackType, 0)
	log.Println(fmt.Sprintf(soundCloudPlaylistTrackURL, playlist.Id, soundCloudClientID))
	resp, err := http.Get(fmt.Sprintf(soundCloudPlaylistTrackURL, playlist.Id, soundCloudClientID))
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	var tracks []soundcloud.Track
	json.NewDecoder(resp.Body).Decode(&tracks)
	for _, track := range tracks {
		trackItem := &commons.TrackType{
			track.Id,
			track.Title,
			track.ArtworkUrl,
			track.StreamUrl,
			"", "", 0, 0,
		}
		createTrackDir(*playlistItem, *trackItem)
		if trackItem.ArtworkURL != "" {
			trackItem.ArtworkFile = fmt.Sprintf(commons.TrackImageFile, playlistItem.ID, trackItem.ID, trackItem.ID)
			downloadChan <- downloadableFile{
				trackItem.ArtworkFile,
				trackItem.ArtworkURL,
			}
		}
		if trackItem.SoundURL != "" {
			trackItem.SoundFile = fmt.Sprintf(commons.TrackSoundFile, playlistItem.ID, trackItem.ID, trackItem.ID)
			trackItem.SoundURL = fmt.Sprintf(trackSoundURL, trackItem.SoundURL, soundCloudClientID)
			downloadChan <- downloadableFile{
				trackItem.SoundFile,
				trackItem.SoundURL,
			}
		}
		tracksList = append(tracksList, trackItem)
	}

	playlistItem.Tracks = tracksList
}

func downloadFile(fileDownloads <-chan downloadableFile, group *sync.WaitGroup, id int) {
	defer group.Done()
	log.Printf("Starting donwload worker id %d\n", id)
	for fileToDownload := range fileDownloads {
		if _, err := os.Stat(fileToDownload.filePath); os.IsNotExist(err) {
			log.Printf("Downloading %s with worker id %d\n", fileToDownload.fileURL, id)
			// Create the file
			out, err := os.Create(fileToDownload.filePath)
			if err != nil {
				panic(err)
			}
			defer out.Close()

			// Get the data
			resp, err := http.Get(fileToDownload.fileURL)
			if err != nil {
				panic(err)
			}
			defer resp.Body.Close()

			// Write the body to file
			_, err = io.Copy(out, resp.Body)
			if err != nil {
				panic(err)
			}
			log.Printf("End of download %s with worker id %d\n", fileToDownload.fileURL, id)
		} else {
			log.Printf("Worker %d - File already exists : %s\n", id, fileToDownload.filePath)
		}
	}
	log.Printf("Stopping worker id %d\n", id)
}
